# Split Preview

This Module provides the Split Feature, Split the preview page on different 
page layouts, mobile, Tablet and Desktop view.

Additionally this module also provide the preview feature on node layout 
builder page as well.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/split_preview).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/split_preview).


## Contents of this file

- Requirements
- Installation
- Configuration
- Usage


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- If your site is managed via Composer, use Composer to download the module.

    1. `composer require drupal/split_preview`
    1. `drush en split_preview`
    1. `drush cr`

  Or

- Install as you would normally install a contributed Drupal module. For further
  information, see
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Enable the module Split Preview go to `Administration > Extend` or by drush 
  command `drush en split_preview`. 
- Login as admin user, Go to Administrator menu => Structure => 
  Content Types => Select Any Content Type => Edit Content Type => 
  Then select the radio button preview optional or required.
- On your content type Live Preview Button Appears.


## Usage

- Instead of opening (default) node preview on independent page, This split 
  preview provides the feature to open that preview at same page (Ajax Load) 
  in a IFrame, Also provides a split previews like Mobile, Tablet and Desktop 
  preview.
